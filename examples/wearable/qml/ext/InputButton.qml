import QtQuick 2.10
import QtQuick.Controls 2.10
import EQL5 1.0

Button {
    width: 33
    height: width
    font.pixelSize: 18
    font.bold: true

    onClicked: Lisp.call("watch:button-clicked", text)
}
