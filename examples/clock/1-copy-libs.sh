# ECL, EQL5 libs

rm -fr   android-sources/libs/armeabi-v7a
mkdir -p android-sources/libs/arm64-v8a

cp ../../lib/libeql5.so       android-sources/libs/arm64-v8a/
cp ../../lib/libeql5_quick.so android-sources/libs/arm64-v8a/
cp $ECL_ANDROID/lib/libecl.so android-sources/libs/arm64-v8a/
