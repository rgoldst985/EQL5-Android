### Info

Please see both the **sokoban** and **REPL** docu in parent directories.

This is an example of how to add a (very simple) REPL to any android app.

The APK is bigger here, because it includes all the contribs needed for
Quicklisp etc., plus a working (patched) Swank version.



### Tips

In addition to the docu mentioned above, you can use this function to load a
file using a file dialog:

```
  (dialogs:load-file)
```

If you only want to select a file, call:

```
  (dialogs:get-file-name)
```

The selected file is stored in `dialogs:*file-name*`.

--

See also **Reload QML files** in [../REPL/README-2](../REPL/README-2-BUILD-APK.md).



### Desktop notes

To run it on the desktop, do:
```
  eql5 run.lisp
```

Using Slime (see docu in desktop EQL5), first run
```
  eql5 ~/slime/eql-start-swank.lisp run.lisp
```
then connect from Emacs `M-x slime-connect`
