(load "../../utils/EQL5-symbols")
(load "../../utils/cross-compile")

(setf *break-on-signals* 'error)

(in-package :eql-user)

(defparameter *files* '("lisp/ui-vars"
                        "lisp/game-logic"
                        "lisp/tic-tac-toe"))

(setf *load-verbose* nil)
(setf *compile-verbose* t)
(setf c::*suppress-compiler-warnings* nil)
(setf c::*suppress-compiler-notes* nil)

(setf c::*compile-in-constants* t)

(trace c::builder)

(push :release *features*)

(defparameter *force-compile* (find "-f" (ext:command-args) :test 'string=))

(dolist (file *files*)
  (let ((src (x:cc file ".lisp"))
        (obj (x:cc file ".o")))
    (print src)
    ;; exclude files using inline C code
    (unless (find (pathname-name src) '(#| nothing yet |#) :test 'string=)
      (load src))
    (when (or *force-compile*
              (> (file-write-date src)
                 (if (probe-file obj)
                     (file-write-date obj)
                     0)))
      (compile-file src :system-p t))))

(c:build-static-library "build/app"
                        :lisp-files (mapcar (lambda (file) (x:cc file ".o"))
                                            *files*)
                        :init-name "ini_app"
                        :epilogue-code '(eql-user::ini))
